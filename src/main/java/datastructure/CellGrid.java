package datastructure;

import cellular.CellState;

import java.awt.*;

public class CellGrid implements IGrid {

    int cols;
    int rows;
    CellState[][] states;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.cols = columns;
        this.states = new CellState[rows][columns];
        for(int r=0; r < rows; r++){
            for(int c=0; c < columns; c++){
                this.states[r][c] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        states[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return states[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid newGrid = new CellGrid(numRows(), numColumns(), CellState.ALIVE);
        for(int r=0; r < numRows(); r++) {
            for (int c = 0; c < numColumns(); c++) {
                newGrid.set(r, c, this.get(r, c));
            }
        }
        return newGrid;
    }
}