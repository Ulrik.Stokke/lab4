package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns){
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    /**
     *
     * Get the state of the cell in the provided row and column
     *
     * @param row    The row of the cell, 0-indexed
     * @param column The column of the cell, 0-indexed
     * @return The state of the cell in the given row and column.
     */
    public CellState getCellState(int row, int column){
        return currentGeneration.get(row,column);
    }

    /**
     * Sets the start-state for each cell
     */
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    /**
     * Updates the state of the cell according to the rules of the automaton
     */
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int r = 0; r < numberOfRows(); r++){
            for (int c = 0; c < numberOfColumns(); c++) {
                nextGeneration.set(r,c, getNextCell(r, c));
            }
        }
        currentGeneration = nextGeneration;
    }

    /**
     * Given the current state of the cell and the number of alive neighbours,
     * decide the next state of the cell.
     *
     * @param row    The row of the cell, 0-indexed
     * @param column The column of the cell, 0-indexed
     * @return state of cell for next generation
     *
     * En levende celle blir døende
     * En døende celle blir død
     * En død celle med akkurat 2 levende naboer blir levende
     * En død celle forblir død ellers
     */
    public CellState getNextCell(int row, int col){
        CellState currentState = this.getCellState(row,col);
        int nAliveNeighbors = countNeighbors(row, col, CellState.ALIVE);

        if (currentState.equals(CellState.ALIVE))
            return CellState.DYING;
        if (currentState.equals(CellState.DYING))
            return CellState.DEAD;
        if (nAliveNeighbors == 2)
            return CellState.ALIVE;
        return CellState.DEAD;
    }

    private int countNeighbors(int row, int col, CellState state) {
        int neighborCount = 0;

        for (int r = row-1; r <= row+1; r++) {
            for (int c = col-1; c <= col+1; c++) {
                if (r >= currentGeneration.numRows() || c >= currentGeneration.numColumns() || r < 0 || c < 0) {
                    continue;
                }
                if (r == row && c == col) {
                    continue;
                }
                else if (state.equals(currentGeneration.get(r, c))) {
                    neighborCount++;
                }
            }
        }
        return neighborCount;
    }

    /**
     * @return The number of rows in this automaton
     */
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    public IGrid getGrid(){
        return currentGeneration;
    }

}
